import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ChicagoService } from "./services/chicago.service";
import { LeafletModule } from "@asymmetrik/ngx-leaflet";
import { LeafletMarkerClusterModule } from "@asymmetrik/ngx-leaflet-markercluster";

import { AppComponent } from './app.component';
import { ChicagoComponent } from './chicago/chicago.component';
import { HttpClientModule } from '@angular/common/http';
import { GraphComponent } from './graph/graph.component';
import { SelectionDateComponent } from './selection-date/selection-date.component';
import { TableauComponent } from './tableau/tableau.component';
import { MapComponent } from './map/map.component';



@NgModule({
  declarations: [
    AppComponent,
    ChicagoComponent,
    GraphComponent,
    SelectionDateComponent,
    TableauComponent,
    MapComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    LeafletModule,
    LeafletMarkerClusterModule
  ],
  providers: [ChicagoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
